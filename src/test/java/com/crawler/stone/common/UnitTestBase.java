package com.crawler.stone.common;

import com.crawler.common.config.JacksonObjectMapper;
import com.crawler.rest.testconfig.AbstractSpringJunit4Config;
import com.crawler.stone.config.AppConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class })
public class UnitTestBase  extends AbstractSpringJunit4Config{
	
	
	@Override
	protected void initUserId() {
		this.setUserId("8");
		
	}
	
	@Override
	public void setUp() {

	}

	protected void print(Object object){
		System.out.println("==============================================================================================");
		try{
			ObjectMapper objectMapper = new JacksonObjectMapper();
			System.out.println(objectMapper.writeValueAsString(object));
        }
        catch(Exception ex){
            System.out.println("无法转为json格式：" + object);
        }
		System.out.println("==============================================================================================");
	}
	


}
