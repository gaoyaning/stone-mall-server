package com.crawler.uc.config;

import com.crawler.listener.RedisPubSubListener;
import com.crawler.uc.listener.UCPubSubListener;
import com.crawler.uc.thread.UcRedisSubThread;

/**
 * Redis 订阅器初始化
 */

public class UcSubscribeInitializer {
	
	/**
	 * 初始化状态
	 */
	private static boolean INIT_STATE = false;
	
	/**
	 * 初始化
	 */
	public synchronized static void init(){
		if(INIT_STATE){
			return ;
		}
		//订阅配置频道
		subscribe(UcConstant.PUSH_CHANNEL, new UCPubSubListener());	
	}
	
	/**
	 * 频道订阅
	 * @param channel
	 * @param listener
	 */
	private synchronized static void subscribe(final String channel, RedisPubSubListener listener){
		UcRedisSubThread subThread = new UcRedisSubThread(listener, channel);
	    subThread.start();
	}

}