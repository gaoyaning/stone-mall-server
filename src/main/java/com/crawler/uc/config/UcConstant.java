package com.crawler.uc.config;

import com.crawler.rest.config.WafProperties;

/**
 * UC常量
 * @author rubekid
 *
 * 2017年9月28日 下午5:37:46
 */
public class UcConstant {

	/**
	 * 是否开启UC
	 */
	public static final boolean UC_ENABLE = WafProperties.getPropertyForBoolean("uc.enable", "false");
	
	/**
	 * 多设备登录
	 */
	public static final boolean MULTI_DEVICE = WafProperties.getPropertyForBoolean("token.multi_device", "false");

	/**
	 * uc uri名称
	 */
	public static final String UC_URI = "uc.uri";
    
	/**
	 * uc host 名称
	 */
	public static final String UC_HOST = "uc.host";
	
	/**
	 * 推送频道
	 */
	public static final String PUSH_CHANNEL = "PUSH_CHANNEL";
	
	/**
	 * 推送事件 ：强退
	 */
	public static final String PUSH_EVENT_KICK = "kick";
}
