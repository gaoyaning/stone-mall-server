package com.crawler.uc.push;

/**
 * 推送事件
 * @author rubekid
 *
 * 2017年10月15日 下午9:21:21
 */
public class PushBody {

	/**
	 * 事件
	 */
	private String event;
	
	/**
	 * 数据
	 */
	private Object data;
	
	public PushBody() {
	}
	
	public PushBody(String event, Object data){
		this.event = event;
		this.data = data;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
