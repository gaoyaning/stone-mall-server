package com.crawler.uc.push.event;

/**
 * 强退事件
 * @author rubekid
 *
 * 2017年10月15日 下午9:35:40
 */
public class KickEvent {
	
	/**
	 * token类型
	 */
	private String type;
	
	/**
	 * token
	 */
	private String accessToken;
	
	public KickEvent() {
	}
	
	public KickEvent(String type, String accessToken){
		this.type = type;
		this.accessToken = accessToken;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
