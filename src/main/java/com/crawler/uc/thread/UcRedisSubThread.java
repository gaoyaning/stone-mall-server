package com.crawler.uc.thread;

import com.crawler.uc.utils.UcRedisManager;
import redis.clients.jedis.JedisPubSub;

/**
 * redis订阅进程
 * @author rubekid
 *
 * 2017年1月5日 下午3:55:23
 */
public class UcRedisSubThread extends Thread {
	
	private JedisPubSub listener;
	
	private String channel;
	
    /**
     * 构造
     * @param listener
     * @param channel
     */
    public UcRedisSubThread(final JedisPubSub listener, final String channel) {
		this.listener = listener;
		this.channel = channel;
	}

    @Override
    public void run() {
        UcRedisManager.subscribe(listener, channel);
    }
}