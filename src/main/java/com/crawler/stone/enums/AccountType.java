package com.crawler.stone.enums;

/**
 * 
 * 账户类型设置
 * @author rubekid
 *
 * 2017年8月1日 下午1:56:34
 */
public enum AccountType {

	main("主账户","main"),
	sub("子账户","sub"),
	increase("增值账户","increase"),
	agent("代理商","agent"),
	operate("业务员","operate"),
	saller("商家","saller");
	/**
     * 名称
     */
    private String name;

    /**
     * 值
     */
    private String value;

    private AccountType(String name, String value) {
        this.name = name;
        this.value = value;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
