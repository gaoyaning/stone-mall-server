package com.crawler.stone.enums;

/**
 * 
 * 规则设置类型
 * @author rubekid
 *
 * 2017年8月1日 下午1:56:34
 */
public enum RuleSettingType {

	stored("预存规则", "stored"),
	release("释放", "release"),
	zrelease("增值账户释放", "zrelease"),
	activateIncreaseAccount("激活增值账户", "activate_increase_account"),
	orderMatch("订单匹配", "order_match"),
	buyReward("采购奖", "buy_reward"),
	thawRatio("解冻比例", "thaw_ratio"),
	scoreRate("积分比例", "score_rate"),
	tradingCurb("交易限制", "trading_curb"),
	fee("费用设置", "fee"),
	maxSellSpace("出售时间间隔","maxSellSpace"),
	scoreValue("积分奖值", "score_value"),
	otherSettingScore("其他设置修改个人资料费用", "other_setting_score"),
	otherSettingServiceTime("系统维护时间", "other_setting_serviceTime"),
	otherSettingCount("统计参数配置", "other_setting_count"),
	agreement("协议设置","agreement"),
	collectScore("收分设置","collect_score");

	/**
     * 名称
     */
    private String name;

    /**
     * 值
     */
    private String value;

    private RuleSettingType(String name, String value) {
        this.name = name;
        this.value = value;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
