package com.crawler.stone.dao;

import com.crawler.stone.model.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author hhd123
 * @since 2018-01-10
 */
public interface UserMapper extends BaseMapper<User> {

}
