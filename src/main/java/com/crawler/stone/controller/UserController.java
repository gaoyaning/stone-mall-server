package com.crawler.stone.controller;


import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author hhd123
 * @since 2018-01-10
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/test")
    public String test(){
        return "123";
    }
}

