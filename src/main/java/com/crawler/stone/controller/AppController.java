package com.crawler.stone.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * App 接口
 * @author rubekid
 *
 * 2017年9月2日 下午1:11:51
 */
@RestController
@RequestMapping(value="/app")
public class AppController {

	/**
	 * 手机端用户主页
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public Map<String,Object> home() {
		Map map = new HashMap();
		map.put("1",1);
		return map;
	}



}
