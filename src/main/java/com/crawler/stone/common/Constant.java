package com.crawler.stone.common;

import com.crawler.component.SpringContextUtil;
import com.crawler.rest.config.WafProperties;

/**
 * 常量
 * @author rubekid
 * @date 2016年7月18日
 */
public class Constant {

	/**
	 * 每日短信限制
	 */
	public static final int SMS_DAY_LIMIT = WafProperties.getPropertyForInteger("sms.day.limit", "50");

	/**
	 * 应用ID
	 */
	public static final String APP_ID = WafProperties.getProperty("app.id");
	
	/**
	 * UC配置
	 */
	public static final String UC_URI = WafProperties.getProperty("uc.uri");
	
	/**
	 * 默认头像
	 */
	public static final String DEFAULT_AVATAR = WafProperties.getProperty("user.avatar", "http://ovjc7q15t.bkt.clouddn.com/avatar.jpg");
	
	/**
	 * 文件保存基础路径
	 */
	public static final String UPLOAD_BASE_PATH = WafProperties.getProperty("upload.basepath", "/data/wwwroot/static/");
	
	/**
	 * 文件保存域名
	 */
	public static final String DOMAIN = WafProperties.getProperty("upload.domain");
	
	/**
	 * 项目路径
	 */
	public static final String SERVER_URL = "http://home.tianji007.com/#/share";
	
	/**
	 * 获取当前服务器根目录
	 * @return
	 */
	public static String getContextPath() {
		return SpringContextUtil.getServletContext().getRealPath("/");
	}
	
	/**
	 * 获取文件保存目录
	 * @param org
	 * @return
	 */
	public static String getAbsolutePath(String path){
		return  getContextPath() + path;
	}
	
	// 定义一些错误提示常量
	public static final String PARAM_ERROR = "参数错误，请刷新重试";
	public static final String SYS_ERROR = "服务器异常，请刷新重试";
	

}
