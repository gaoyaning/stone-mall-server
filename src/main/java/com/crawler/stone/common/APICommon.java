package com.crawler.stone.common;

import com.crawler.utils.UcUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * api层通用类
 * Created by Mr.Liu
 * on 2017/7/26.
 */
public class APICommon {
    /**
     * 设置分页数据
     * @param anonymous 是否游客可以访问
     * @return
     */
    public static Map<String,Object> getPageMap(Integer pageNo,Integer pageSize,String orderBy,boolean anonymous){
        Map<String,Object> map=new HashMap<>();
        map.put("startIndex",(pageNo-1)*pageSize);
        map.put("pageNo",pageNo);
        map.put("pageSize",pageSize);
        if(!StringUtils.isEmpty(orderBy)){
            String [] arr=orderBy.split(" ");
            if(arr!=null &&arr.length==2){
                StringBuffer sb=new StringBuffer();
                sb.append("a.");
                sb.append(arr[0].trim()+" ");
                sb.append(arr[1]);
                map.put("orderBy",sb.toString());
            }
        }
        map.put("userId",UcUtils.getUserId(anonymous));
        return map;
    }

}
