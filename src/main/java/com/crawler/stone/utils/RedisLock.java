package com.crawler.stone.utils;


import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class RedisLock{

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisLock.class);

    private static final int DEFAULT_SINGLE_EXPIRE_TIME = 6;
    
    private static final int DEFAULT_BATCH_EXPIRE_TIME = 6;

    
    /**
     * 构造
     * @author http://blog.csdn.net/java2000_wl
     */
   /* public RedisBillLockHandler(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }*/

    /**
     * 获取锁  如果锁可用   立即返回true，  否则返回false
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBillLockHandler#tryLock(com.fx.platform.components.lock.IBillIdentify)
     * @param billIdentify
     * @return
     */
    public static boolean tryLock(String key) {
        return tryLock(key, 0L, null);
    }

    /**
     * 锁在给定的等待时间内空闲，则获取锁成功 返回true， 否则返回false
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBillLockHandler#tryLock(com.fx.platform.components.lock.IBillIdentify,
     *      long, java.util.concurrent.TimeUnit)
     * @param billIdentify
     * @param timeout
     * @param unit
     * @return
     */
    public static boolean tryLock(String key, long timeout, TimeUnit unit) {
    	 return tryLock(key, timeout, unit, DEFAULT_SINGLE_EXPIRE_TIME);
    }
    
    /**
     * 锁在给定的等待时间内空闲，则获取锁成功 返回true， 否则返回false
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBillLockHandler#tryLock(com.fx.platform.components.lock.IBillIdentify,
     *      long, java.util.concurrent.TimeUnit)
     * @param billIdentify
     * @param timeout
     * @param unit
     * @return
     */
    public static boolean tryLock(String key, long timeout, TimeUnit unit, int expireTime) {
    	 RedisUtil redis = RedisUtil.getInstance();
         Jedis jedis = null;
        try {
        	jedis = redis.getJedis();
            long nano = System.nanoTime();
            do {
                //System.out.println("try lock key: " + key);
                Long i = jedis.setnx(key, key);
                if (i == 1) {
                	if(expireTime > 0){                		
                		jedis.expire(key, expireTime);
                	}
                    //System.out.println("获取锁, key: " + key + " , expire in " + DEFAULT_SINGLE_EXPIRE_TIME + " seconds.");
                    return Boolean.TRUE;
                } else { // 存在锁
                    
                    String desc = jedis.get(key);
                    System.out.println("已被其他用户锁住key: " + key + " 已被其他用户锁住：" + desc);
                    
                }
                if (timeout == 0) {
                    break;
                }
                Thread.sleep(10);
            } while ((System.nanoTime() - nano) < unit.toNanos(timeout));
            return Boolean.FALSE;
        } catch (JedisConnectionException je) {
            LOGGER.error(je.getMessage(), je);
          //  returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
         //   returnResource(jedis);
        	if(jedis!=null){
        		try{        			
        			jedis.close();
        		}
        		catch(Exception ex){
        			LOGGER.error(ex.getMessage(), ex);
        		}
        	}
        }
        return Boolean.FALSE;
    }

    /**
     * 如果锁空闲立即返回   获取失败 一直等待
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBillLockHandler#lock(com.fx.platform.components.lock.IBillIdentify)
     * @param billIdentify
     */
    public static void lock(String  key) {
    	RedisUtil redis = RedisUtil.getInstance();
        Jedis jedis = null;
        try {
        	jedis = redis.getJedis();
            do {
                LOGGER.debug("lock key: " + key);
                Long i = jedis.setnx(key, key);
                if (i == 1) { 
                    jedis.expire(key, DEFAULT_SINGLE_EXPIRE_TIME);
                    LOGGER.debug("get lock, key: " + key + " , expire in " + DEFAULT_SINGLE_EXPIRE_TIME + " seconds.");
                    return;
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        String desc = jedis.get(key);
                        LOGGER.debug("key: " + key + " locked by another business：" + desc);
                    }
                }
                Thread.sleep(300); 
            } while (true);
        } catch (JedisConnectionException je) {
            LOGGER.error(je.getMessage(), je);
          //  returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
          //  returnResource(jedis);
        	jedis.close();
        }
    }

    /**
     * 释放锁
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBillLockHandler#unLock(com.fx.platform.components.lock.IBillIdentify)
     * @param billIdentify
     */
    public static void unLock(String key) {
    	
    	RedisUtil redis = RedisUtil.getInstance();
        Jedis jedis = null;
        try {
        	jedis = redis.getJedis();
            jedis.del(key);
            LOGGER.debug("释放锁release lock, keys :" + key);
        } catch (JedisConnectionException je) {
            LOGGER.error(je.getMessage(), je);
          //  returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
           // returnResource(jedis);
        	jedis.close();
        }
    	
        /*List<String> list = new ArrayList<String>();
        list.add(key);
        unLock(list);*/
    }

    /**
     * 批量获取锁  如果全部获取   立即返回true, 部分获取失败 返回false
     * @author http://blog.csdn.net/java2000_wl
     * @date 2013-7-22 下午10:27:44
     * @see com.fx.platform.components.lock.IBatchBillLockHandler#tryLock(java.util.List)
     * @param billIdentifyList
     * @return
     */
    public static boolean tryLock(List<String> keyList) {
        return tryLock(keyList, 0L, null);
    }
    
    /**
     * 锁在给定的等待时间内空闲，则获取锁成功 返回true， 否则返回false
     * @author http://blog.csdn.net/java2000_wl
     * @param billIdentifyList
     * @param timeout
     * @param unit
     * @return
     */
    public static boolean tryLock(List<String> keyList, long timeout, TimeUnit unit) {
    	RedisUtil redis = RedisUtil.getInstance();
        Jedis jedis = null;
    	try {
    		jedis = redis.getJedis();
            List<String> needLocking = new CopyOnWriteArrayList<String>();    
            List<String> locked = new CopyOnWriteArrayList<String>();    
            long nano = System.nanoTime();
            do {
                // 构建pipeline，批量提交
                Pipeline pipeline = jedis.pipelined();
                for (String key : keyList) {
                    needLocking.add(key);
                    pipeline.setnx(key, key);
                }
                LOGGER.debug("try lock keys: " + needLocking);
                // 提交redis执行计数
                List<Object> results = pipeline.syncAndReturnAll();
                for (int i = 0; i < results.size(); ++i) {
                    Long result = (Long) results.get(i);
                    String key = needLocking.get(i);
                    if (result == 1) {    // setnx成功，获得锁
                        jedis.expire(key, DEFAULT_BATCH_EXPIRE_TIME);
                        locked.add(key);
                    } 
                }
                needLocking.removeAll(locked);    // 已锁定资源去除
                
                if (CollectionUtils.isEmpty(needLocking)) {
                    return true;
                } else {    
                    // 部分资源未能锁住
                    LOGGER.debug("keys: " + needLocking + " locked by another business：");
                }
                
                if (timeout == 0) {    
                    break;
                }
                Thread.sleep(500);    
            } while ((System.nanoTime() - nano) < unit.toNanos(timeout));

            // 得不到锁，释放锁定的部分对象，并返回失败
            if (!CollectionUtils.isEmpty(locked)) {
                jedis.del(locked.toArray(new String[0]));
            }
            return false;
        } catch (JedisConnectionException je) {
            LOGGER.error(je.getMessage(), je);
         //   returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
         //   returnResource(jedis);
        	jedis.close();
        }
        return true;
    }

    /**
     * 批量释放锁
     * @author http://blog.csdn.net/java2000_wl
     * @see com.fx.platform.components.lock.IBatchBillLockHandler#unLock(java.util.List)
     * @param billIdentifyList
     */
    public static void unLock(List<String> keyList) {
        
    	List<String> keys = new CopyOnWriteArrayList<String>();
        for (String key : keyList) {
            keys.add(key);
        }
        RedisUtil redis = RedisUtil.getInstance();
        Jedis jedis = null;
        try {
        	jedis = redis.getJedis();
            jedis.del(keys.toArray(new String[0]));
            LOGGER.debug("释放锁release lock, keys :" + keys);
        } catch (JedisConnectionException je) {
            LOGGER.error(je.getMessage(), je);
          //  returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
           // returnResource(jedis);
        	jedis.close();
        }
    }
    
    /**
     * @author http://blog.csdn.net/java2000_wl
     * @date 2013-7-22 下午9:33:45
     * @return
     */
    /*private Jedis getResource() {
        return jedisPool.getResource();
    }*/
    
    /**
     * 销毁连接
     * @author http://blog.csdn.net/java2000_wl
     * @param jedis
     */
    /*private void returnBrokenResource(Jedis jedis) {
        if (jedis == null) {
            return;
        }
        try {
            //容错
            jedisPool.returnBrokenResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }*/
    
    /**
     * @author http://blog.csdn.net/java2000_wl
     * @param jedis
     */
    /*private void returnResource(Jedis jedis) {
        if (jedis == null) {
            return;
        }
        try {
            jedisPool.returnResource(jedis);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }*/
}