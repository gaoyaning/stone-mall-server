package com.crawler.stone.exception;

import com.crawler.rest.exceptions.extendExceptions.RestSimpleException;
import org.springframework.http.HttpStatus;

/**
 * 统一错误异常处理
 * @author rubekid
 * @date 2016年7月25日
 */
public class SimpleException extends RestSimpleException{
	
	private static String code="REST/BAD_REQUEST";
	
    private static HttpStatus status = HttpStatus.BAD_REQUEST;
	
	public SimpleException(HttpStatus status, String code, String message) {
		super(status, code, message);
	}
	
	public SimpleException(String message) {
		super(status, code, message);
	}

}
