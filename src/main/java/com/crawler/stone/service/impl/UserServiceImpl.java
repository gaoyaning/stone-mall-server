package com.crawler.stone.service.impl;

import com.crawler.stone.model.User;
import com.crawler.stone.dao.UserMapper;
import com.crawler.stone.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author hhd123
 * @since 2018-01-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
