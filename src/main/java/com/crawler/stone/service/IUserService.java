package com.crawler.stone.service;

import com.crawler.stone.model.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author hhd123
 * @since 2018-01-10
 */
public interface IUserService extends IService<User> {

}
