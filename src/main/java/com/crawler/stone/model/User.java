package com.crawler.stone.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author hhd123
 * @since 2018-01-10
 */
@TableName("s_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    /**
     * id/非自增字段
     */
    private Long id;
    /**
     * 加入类型 0直推 1团队
     */
    @TableField("join_type")
    private Integer joinType;
    /**
     * 账户类型 sub :子账户 main:主账户 increase:增值账户 agent 代理商 operate 业务员 saller商家
     */
    @TableField("account_type")
    private String accountType;
    /**
     * 用户名(登录名)
     */
    @TableField("login_name")
    private String loginName;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 保密 0 女 1 男 
     */
    private Integer gender;
    /**
     * 交易密码
     */
    @TableField("trade_password")
    private String tradePassword;
    private String salt;
    /**
     * 用户等级 0普通会员 1-6(vip1-vip6) 
     */
    private Integer grade;
    /**
     * 信用 1一颗星 5满星
     */
    private Integer credit;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 昵称拼音
     */
    @TableField("name_py")
    private String namePy;
    /**
     * 真实名字
     */
    @TableField("real_name")
    private String realName;
    /**
     * 身份证号码
     */
    @TableField("id_card")
    private String idCard;
    /**
     *  推荐人id
     */
    @TableField("ref_id")
    private Long refId;
    /**
     * 所有父级id（不包括增值账户）
     */
    @TableField("ref_ids")
    private String refIds;
    /**
     * 所有父级id(不包括直推)
     */
    @TableField("ref_pids")
    private String refPids;
    /**
     * 激活日期
     */
    @TableField("activate_date")
    private Date activateDate;
    /**
     * 开户名
     */
    @TableField("account_name")
    private String accountName;
    /**
     * 开户行
     */
    @TableField("bank_name")
    private String bankName;
    /**
     * 账户
     */
    @TableField("account_no")
    private String accountNo;
    /**
     * 开户地
     */
    @TableField("account_place")
    private String accountPlace;
    /**
     * 剩余购买力
     */
    @TableField("remain_power")
    private BigDecimal remainPower;
    /**
     * 团队人数
     */
    @TableField("team_num")
    private Integer teamNum;
    /**
     * 子账户数
     */
    @TableField("sub_account_num")
    private Integer subAccountNum;
    /**
     * 增值账户数
     */
    @TableField("increase_account_num")
    private Integer increaseAccountNum;
    /**
     * 注册时间 
     */
    @TableField("create_date")
    private Date createDate;
    /**
     * 更新时间
     */
    @TableField("update_date")
    private Date updateDate;
    /**
     * 最后登录时间
     */
    @TableField("last_login_date")
    private Date lastLoginDate;
    /**
     * 是否激活 0 否 1是
     */
    private Integer activated;
    /**
     * 是否冻结 0 否 1 是
     */
    private Integer locked;
    /**
     * 修改次数
     */
    @TableField("alter_count")
    private Integer alterCount;
    /**
     * 扩展积分1
     */
    private BigDecimal extend1;
    /**
     * 扩展积分2
     */
    private BigDecimal extend2;
    /**
     * 扩展积分3
     */
    private BigDecimal extend3;
    /**
     * 扩展积分4
     */
    private BigDecimal extend4;
    /**
     * 扩展积分5
     */
    private BigDecimal extend5;
    /**
     * 扩展积分6
     */
    private BigDecimal extend6;
    /**
     * 扩展积分7
     */
    private BigDecimal extend7;
    /**
     * 扩展积分8
     */
    private BigDecimal extend8;
    /**
     * 扩展积分9
     */
    private BigDecimal extend9;
    /**
     * 扩展积分10
     */
    private BigDecimal extend10;
    /**
     * 是否删除 
     */
    @TableField("is_del")
    private Integer isDel;
    /**
     * 是否对碰过，0否 1是
     */
    @TableField("is_touch")
    private Integer isTouch;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getJoinType() {
        return joinType;
    }

    public void setJoinType(Integer joinType) {
        this.joinType = joinType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getTradePassword() {
        return tradePassword;
    }

    public void setTradePassword(String tradePassword) {
        this.tradePassword = tradePassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNamePy() {
        return namePy;
    }

    public void setNamePy(String namePy) {
        this.namePy = namePy;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getRefIds() {
        return refIds;
    }

    public void setRefIds(String refIds) {
        this.refIds = refIds;
    }

    public String getRefPids() {
        return refPids;
    }

    public void setRefPids(String refPids) {
        this.refPids = refPids;
    }

    public Date getActivateDate() {
        return activateDate;
    }

    public void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountPlace() {
        return accountPlace;
    }

    public void setAccountPlace(String accountPlace) {
        this.accountPlace = accountPlace;
    }

    public BigDecimal getRemainPower() {
        return remainPower;
    }

    public void setRemainPower(BigDecimal remainPower) {
        this.remainPower = remainPower;
    }

    public Integer getTeamNum() {
        return teamNum;
    }

    public void setTeamNum(Integer teamNum) {
        this.teamNum = teamNum;
    }

    public Integer getSubAccountNum() {
        return subAccountNum;
    }

    public void setSubAccountNum(Integer subAccountNum) {
        this.subAccountNum = subAccountNum;
    }

    public Integer getIncreaseAccountNum() {
        return increaseAccountNum;
    }

    public void setIncreaseAccountNum(Integer increaseAccountNum) {
        this.increaseAccountNum = increaseAccountNum;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getActivated() {
        return activated;
    }

    public void setActivated(Integer activated) {
        this.activated = activated;
    }

    public Integer getLocked() {
        return locked;
    }

    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    public Integer getAlterCount() {
        return alterCount;
    }

    public void setAlterCount(Integer alterCount) {
        this.alterCount = alterCount;
    }

    public BigDecimal getExtend1() {
        return extend1;
    }

    public void setExtend1(BigDecimal extend1) {
        this.extend1 = extend1;
    }

    public BigDecimal getExtend2() {
        return extend2;
    }

    public void setExtend2(BigDecimal extend2) {
        this.extend2 = extend2;
    }

    public BigDecimal getExtend3() {
        return extend3;
    }

    public void setExtend3(BigDecimal extend3) {
        this.extend3 = extend3;
    }

    public BigDecimal getExtend4() {
        return extend4;
    }

    public void setExtend4(BigDecimal extend4) {
        this.extend4 = extend4;
    }

    public BigDecimal getExtend5() {
        return extend5;
    }

    public void setExtend5(BigDecimal extend5) {
        this.extend5 = extend5;
    }

    public BigDecimal getExtend6() {
        return extend6;
    }

    public void setExtend6(BigDecimal extend6) {
        this.extend6 = extend6;
    }

    public BigDecimal getExtend7() {
        return extend7;
    }

    public void setExtend7(BigDecimal extend7) {
        this.extend7 = extend7;
    }

    public BigDecimal getExtend8() {
        return extend8;
    }

    public void setExtend8(BigDecimal extend8) {
        this.extend8 = extend8;
    }

    public BigDecimal getExtend9() {
        return extend9;
    }

    public void setExtend9(BigDecimal extend9) {
        this.extend9 = extend9;
    }

    public BigDecimal getExtend10() {
        return extend10;
    }

    public void setExtend10(BigDecimal extend10) {
        this.extend10 = extend10;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getIsTouch() {
        return isTouch;
    }

    public void setIsTouch(Integer isTouch) {
        this.isTouch = isTouch;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
        ", id=" + id +
        ", joinType=" + joinType +
        ", accountType=" + accountType +
        ", loginName=" + loginName +
        ", avatar=" + avatar +
        ", gender=" + gender +
        ", tradePassword=" + tradePassword +
        ", salt=" + salt +
        ", grade=" + grade +
        ", credit=" + credit +
        ", email=" + email +
        ", phone=" + phone +
        ", nickName=" + nickName +
        ", namePy=" + namePy +
        ", realName=" + realName +
        ", idCard=" + idCard +
        ", refId=" + refId +
        ", refIds=" + refIds +
        ", refPids=" + refPids +
        ", activateDate=" + activateDate +
        ", accountName=" + accountName +
        ", bankName=" + bankName +
        ", accountNo=" + accountNo +
        ", accountPlace=" + accountPlace +
        ", remainPower=" + remainPower +
        ", teamNum=" + teamNum +
        ", subAccountNum=" + subAccountNum +
        ", increaseAccountNum=" + increaseAccountNum +
        ", createDate=" + createDate +
        ", updateDate=" + updateDate +
        ", lastLoginDate=" + lastLoginDate +
        ", activated=" + activated +
        ", locked=" + locked +
        ", alterCount=" + alterCount +
        ", extend1=" + extend1 +
        ", extend2=" + extend2 +
        ", extend3=" + extend3 +
        ", extend4=" + extend4 +
        ", extend5=" + extend5 +
        ", extend6=" + extend6 +
        ", extend7=" + extend7 +
        ", extend8=" + extend8 +
        ", extend9=" + extend9 +
        ", extend10=" + extend10 +
        ", isDel=" + isDel +
        ", isTouch=" + isTouch +
        "}";
    }
}
