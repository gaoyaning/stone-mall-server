package com.crawler.stone.config.security;

import com.crawler.rest.config.RestWebSecurityConfigurerAdapter;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;


/**
 * 
 */
@Configuration
@EnableWebMvcSecurity
public class WafSecurityConfig extends RestWebSecurityConfigurerAdapter {
	
	private static final Logger LOGGER = Logger.getLogger(WafSecurityConfig.class);
	
	
	/**
	 * 配置安全访问控制
	 * 
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void onConfigure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		//登录 注册
		.antMatchers(HttpMethod.POST, "/**/login").permitAll()
		.antMatchers(HttpMethod.POST, "/**/register").permitAll()
		.antMatchers(HttpMethod.GET, "/**/admin/register").permitAll()
		.antMatchers(HttpMethod.PUT, "/**").permitAll()
		.antMatchers(HttpMethod.GET, "/**").permitAll()
		.antMatchers(HttpMethod.POST, "/uploader/**").permitAll()
		.antMatchers(HttpMethod.POST, "/appUser/registerMain").permitAll()
		.antMatchers(HttpMethod.POST, "/captcha/**").permitAll()
		.antMatchers(HttpMethod.GET, "/appUser/getCode/**").permitAll()
		.antMatchers(HttpMethod.GET, "/appUser/adminUpdateTra").permitAll()
		.anyRequest().authenticated();
		
	}
	
	/**
	 * 此方法默认当前应用全部忽略安全配置，如需使得安全机制有效，重写此方法，设置忽略路径等。
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

}
