package com.crawler.stone.config;

import com.crawler.rest.AbstractRestWebApplicationInitializer;
import com.crawler.stone.config.security.WafSecurityConfig;
import com.crawler.uc.config.UcConstant;
import com.crawler.uc.config.UcSubscribeInitializer;
import org.springframework.orm.hibernate4.support.OpenSessionInViewFilter;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * web项目的启动配置
 *
 *
 */

public class WebApplicationInitializer extends AbstractRestWebApplicationInitializer{


    public void registerFilters(ServletContext servletContext) {
        OpenSessionInViewFilter openSessionInViewFilter = new OpenSessionInViewFilter();
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter("openSessionInViewFilter",
                openSessionInViewFilter);
        filterRegistration.setAsyncSupported(isAsyncSupported());
        filterRegistration.addMappingForUrlPatterns(getDispatcherTypes(), false, "/*");
    }

    // 设置全局配置，包括安全以及数据仓储等
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { WafSecurityConfig.class, AppConfig.class };
    }

    // 设置web配置类
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebMVCConfig.class , WebSupportConfig.class};
    }	
    
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		if(!UcConstant.MULTI_DEVICE){
			UcSubscribeInitializer.init();
		}
	}

}